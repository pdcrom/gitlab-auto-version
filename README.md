# Automatic versioning of Gitlab projects

A docker image is presented here that can be used to automatically version GitLab projects.
It creates git tags for commits with format \<major\>.\<minor\>.\<patch\>, e.g. "1.0.0"

The logic for creating versions works as follows:
- If no git tag for the project is present, a tag '1.0.0' will be created
- If a merge request id is found in the commit message of format "\<group\>/\<project>\!\<merge_request_id\>" then it will check for the following labels on the merge request:
    - 'bump-major', if found bump major version
    - 'bump-minor', if found bump minor version
  When no labels are found, the patch version is bumped
- If no merge request is found for the commit then the patch version is bumped

## Requirements

The logic relies on the fact that the merge request id can be found in the commit message. When creating a merge request, under "Merge options", select "Squash commits when merge request is accepted.", it will create a commit message that includes a line "See merge request \<group\>/\<project\>!\<merge_request_id\>".

The docker image makes use of the GitLab API and needs a Personal Access Tokens to work. Create a user that has access to the desired repositories and create a Personal Access Token for it (allow access to the API)

On the project (or group) under Settings -> CI/CD -> Variables, add a variable "GITLAB_TOKEN" with the value of the Personal Access Tokens created above.

## Usage in a pipeline

Here's an example .gitlab-ci.yml that uses the automatic tagging feature.
There are two jobs:
- version: runs on master branch and creates a git tag with a version for the new commit
- build: runs when a tag is added and builds a new docker image with the version created in the git tag

The pipeline below does not prevent to run when a 'tag' is created manually on a branch that is not the master


```
image: registry.gitlab.com/pdcrom/gitlab-ci-docker:latest

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_TLSCERTDIR: "/certs"
  IMAGE_NAME: $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME

services:
- docker:dind

.before_script_template:
  before_script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY

build-dev:
  extends: .before_script_template
  stage: build
  only:
  - dev
  script:
    - docker build -t ${IMAGE_NAME}:dev-$CI_COMMIT_SHORT_SHA  .
    - docker push ${IMAGE_NAME}:dev-$CI_COMMIT_SHORT_SHA 

build-prod:
  extends: .before_script_template
  stage: build
  only:
    - master 
  script:
    - python3 /version-update/version-update.py
    - TAG=$(cat version.txt)
    - docker build -t ${IMAGE_NAME}:${TAG}  .
    - docker push ${IMAGE_NAME}:${TAG}
    - docker tag ${IMAGE_NAME}:${TAG} ${IMAGE_NAME}:latest
    - docker push ${IMAGE_NAME}:latest
```