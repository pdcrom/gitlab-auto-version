FROM python:3.8-alpine

WORKDIR /version-update

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY version-update.py .

CMD ["python", "version-update.py"]
